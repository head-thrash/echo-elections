package ru.bkmz.echo

import akka.testkit.{TestProbe, ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}
import scala.collection.immutable
import scala.concurrent.duration._
import org.scalatest.{WordSpecLike, Matchers, BeforeAndAfterAll}
import ru.bkmz.echo.Messages._

@org.junit.runner.RunWith(classOf[org.scalatest.junit.JUnitRunner])
class NodeSpecs(_system: ActorSystem) extends TestKit(_system)
with ImplicitSender
with WordSpecLike
with Matchers
with BeforeAndAfterAll {

  val timeout: FiniteDuration = 2.seconds

  def this() = this(ActorSystem("NodeSpecifications"))

  override def afterAll() {
    TestKit.shutdownActorSystem(_system)
  }

  "Node" must {

    "do active-finethanks exchange" in {
      val node = system.actorOf(Props[Node])
      node ! Init(immutable.Seq())
      node ! Alive
      expectMsg(timeout, FineThanks)
    }

    "ping a king" in {
      val node = system.actorOf(Props[Node])
      val king = TestProbe()
      node ! Init(immutable.Seq(king.ref))
      king.expectMsg(timeout, Alive)
      king.reply(FineThanks)
      king.reply(ImTheKing(king.ref))
      king.expectMsg(timeout, Alive)
    }

    "become a king if no reply from super" in {
      val node = system.actorOf(Props[Node])
      val king = TestProbe()
      node ! Init(immutable.Seq(king.ref))
      king.expectMsg(timeout, Alive)
      king.reply(FineThanks)
      king.expectMsg(timeout, ImTheKing(node))
      king.reply(Alive)
      king.expectMsg(timeout, FineThanks)
    }

    "become a king if no one is active" in {
      val node = system.actorOf(Props[Node])
      val king = TestProbe()
      node ! Init(immutable.Seq(king.ref))
      king.expectMsg(timeout, Alive)
      king.expectMsg(timeout, ImTheKing(node))
    }

    "obey new king even if king itself" in {
      val node = system.actorOf(Props[Node])
      val king = TestProbe()
      val king2 = TestProbe()
      node ! Init(immutable.Seq(king.ref, king2.ref))
      king.expectMsg(timeout, Alive)
      king.expectMsg(timeout, ImTheKing(node))
      king.reply(ImTheKing(king.ref))
      king.expectMsg(timeout, Alive)
      king.reply(FineThanks)
      king.expectMsg(timeout, Alive)
      king2.send(node, ImTheKing(king2.ref))
      king2.expectMsg(timeout, Alive)
    }

    "init a new elections if not a king (and win them)" in {
      val node = system.actorOf(Props[Node])
      val king = TestProbe()
      node ! Init(immutable.Seq(king.ref))
      node ! Alive
      expectMsg(timeout, FineThanks)
      king.expectMsg(timeout, Alive)
      king.reply(FineThanks)
      king.expectMsg(timeout, ImTheKing(node))
    }

    "reelect new king if old died" in {
      val node1 = system.actorOf(Props[Node])
      val node2 = system.actorOf(Props[Node])
      val king = TestProbe()
      val seq = immutable.Seq(node1, node2, king.ref)

      node1 ! Init(seq)
      node2 ! Init(seq)

      king.send(node1, ImTheKing(king.ref))
      king.send(node2, ImTheKing(king.ref))

      king.fishForMessage(timeout * 10) {
        case Alive => true
        case ImTheKing(_) => true
      }
    }
  }
}
