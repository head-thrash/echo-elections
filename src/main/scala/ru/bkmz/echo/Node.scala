package ru.bkmz.echo

import akka.actor._
import scala.collection.immutable
import scala.concurrent.duration._

sealed trait State

sealed trait Data

object NodeData {

  case object InitMe extends Data

  case class Nodes(list: immutable.List[ActorRef]) extends Data

  case class NewKing(list: immutable.List[ActorRef], r: ActorRef) extends Data

}

object States {

  case object Idle extends State

  case object King extends State

  case object Electing extends State

  case object AskAlives extends State

  case object WaitForApproval extends State

  case object Pinging extends State

  case object WaitPing extends State

}

object Messages {

  case object Alive

  case object FineThanks

  case class ImTheKing(r: ActorRef)

}

class Node extends Actor with LoggingFSM[State, Data] {

  val T: FiniteDuration = 1.second

  import ru.bkmz.echo.States._
  import ru.bkmz.echo.Messages._
  import ru.bkmz.echo.NodeData._

  startWith(Idle, InitMe)

  when(Idle) {
    case Event(Init(nodes), InitMe) => {
      log.info("Starting elections...")
      goto(Electing) using Nodes(nodes.toList)
    }
  }

  when(Electing, stateTimeout = T) {
    case Event(StateTimeout, nodes: Nodes) => {
      log.debug("Getting to ask Alives")
      if (getSuperNodes(nodes.list).isEmpty) {
        log.info("I'm the eldest one, so I'll be ye king.")
        goto(King)
      }
      else {
        goto(AskAlives) using nodes
      }
    }
    case Event(Alive, nodes: Nodes) => {
      sender ! FineThanks
      stay using nodes
    }
  }

  when(AskAlives, stateTimeout = T) {
    case Event(FineThanks, nodes: Nodes) => {
      log.info("Waiting approval from king candidate...")
      goto(WaitForApproval) using nodes
    }
    case Event(StateTimeout, nodes: Nodes) => {
      log.info("Everyone is dead, so I'm the king now!")
      goto(King)
    }
  }

  when(WaitForApproval, stateTimeout = T) {
    case Event(StateTimeout, nodes: Nodes) => {
      log.info("Approval timeouted. I'm the king! :-D")
      goto(King)
    }
  }

  when(Pinging, stateTimeout = T) {
    case Event(StateTimeout, king: NewKing) => {
      log.debug("Okay ping the king.")
      goto(WaitPing) using king
    }
  }

  when(WaitPing, stateTimeout = T * 4) {
    case Event(FineThanks, king: NewKing) => {
      log.debug("All hail the king!")
      goto(Pinging) using king
    }
    case Event(StateTimeout, king: NewKing) => {
      log.info("King had timeouted! Elections!")
      goto(Electing) using Nodes(king.list)
    }
  }

  when(King) {
    case Event(Alive, data: Data) => {
      sender ! FineThanks
      stay using data
    }
  }

  whenUnhandled {
    case Event(ImTheKing(r: ActorRef), nodes: Nodes) => {
      log.info("We obey the new king.")
      goto(Pinging) using NewKing(nodes.list, r)
    }
    case Event(ImTheKing(r: ActorRef), newKing: NewKing) => {
      log.info("New king?! I will revenge.")
      goto(Pinging) using NewKing(newKing.list, r)
    }
    case Event(Alive | FineThanks | AskAlives, _) => {
      stay() // whatever
    }
  }

  onTransition {
    case Pinging -> WaitPing => {
      stateData match {
        case NewKing(nodes, king) => {
          king ! Alive
        }
      }
    }
    case Electing -> AskAlives => {
      stateData match {
        case Nodes(actors) => {
          val superNodes = getSuperNodes(actors)
          superNodes.foreach(x => x ! Alive)
        }
      }
    }
    case _ -> King => {
      stateData match {
        case Nodes(actors) => {
          actors.foreach(x => x ! ImTheKing(self))
        }
      }
    }
  }

  initialize()

  def getSuperNodes(nodes: Seq[ActorRef]) = {
    nodes.filter(x => x.compareTo(self) > 0)
  }
}
