package ru.bkmz.echo

import akka.actor.{ActorRef, Props, ActorSystem}
import scala.collection.immutable

case class Init(nodes: immutable.Seq[ActorRef])

object Main extends App {

  val system = ActorSystem("actors")

  val nodes = for (i <- 1 to 5) yield system.actorOf(Props[Node], s"node_$i")

  nodes.foreach(x => x ! Init(nodes.filterNot(y => y.eq(x))))

  system.awaitTermination()

  Console.println("Bye!")
}
